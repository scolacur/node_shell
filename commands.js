var fs = require('fs');
var request = require('request');

function ls(doneFunc, arg){
	fs.readdir('./', function(err, files){
		if (err) throw err;
		files.forEach(function(file){
			process.stdout.write(file.toString() + "\n");
		});
		//console.log(answer);
		doneFunc();
	});
}

function pwd(doneFunc, arg){
	console.log(process.cwd());
	doneFunc();
}

function date(doneFunc, arg){
	console.log(new Date());
	doneFunc();
}

function echo(doneFunc, arg){
	if (arg){
		console.log(arg);
	}
	doneFunc();
}

function cat(doneFunc, arg){
	var path = "./" + arg;
	fs.readFile(path, "utf8",function(err, data){
		if (err) throw err;
		console.log(data);
		doneFunc();
	});
}

function head(doneFunc, arg){
	var path = "./" + arg;
	fs.readFile(path, "utf8",function(err, data){
		if (err) throw err;
		var dataArr = data.split('\n');
		for (var i=0; i<5; i++){
			console.log(dataArr[i]);
		}
		doneFunc();
	});
}

function tail(doneFunc, arg){
	var path = "./" + arg;
	fs.readFile(path, "utf8",function(err, data){
		if (err) throw err;
		var dataArr = data.split('\n');
		for (var i=dataArr.length-5; i<dataArr.length; i++){
			console.log(dataArr[i]);
		}
		doneFunc();
	});
}

function sort(doneFunc, arg) {
	var path = "./" + arg;
	fs.readFile(path, "utf8",function(err, data){
		if (err) throw err;
		var dataArr = data.split('\n');
		dataArr.sort();
		for (var i=0; i<dataArr.length; i++){
			console.log(dataArr[i]);
		}
		doneFunc();
	});
}

//COMMENT
//COMMENT
//COMMENT

function WC(doneFunc, arg) {
	var path = "./" + arg;
	fs.readFile(path, "utf8",function(err, data){
		if (err) throw err;
		var dataArr = data.split('\n');
		console.log(dataArr.length);
		doneFunc();
	});
}

function UNIQ(doneFunc, arg) {
	var path = "./" + arg;
	fs.readFile(path, "utf8",function(err, data){
		if (err) throw err;
		var dataArr = data.split('\n');
		for (var i=0; i<dataArr.length; i++){
			if (dataArr[i] != dataArr[i-1]){
				console.log(dataArr[i]);
			}
		}
		doneFunc();
	});
}

function curl(doneFunc, arg){
	request(arg, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log(body); // Show the HTML for the Google homepage.
	  	}
	doneFunc();
	});
}

//ANOTHERCOMMENT
//ANOTHERCOMMENT

module.exports = {
  pwd: pwd,
  ls: ls,
  date: date,
  echo: echo,
  cat: cat,
  head: head,
  tail: tail,
  sort: sort,
  WC: WC,
  UNIQ: UNIQ,
  curl: curl
};