var commands = require('./commands');
// var commandName = process.argv[2];
// var commandArgs = process.argv[3];

// Output a prompt
process.stdout.write('prompt > ');

// The stdin 'data' event fires after a user types in a line
process.stdin.on('data', function(data) {
  var cmd = data.toString().trim(); // remove the newline
  var cmdArr = cmd.split(" ");

  function done(){
  	process.stdout.write('\nprompt > ');
  }

  commands[cmdArr[0]](done, cmdArr[1]);  
});




